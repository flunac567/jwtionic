const User= require('../models/user');
const jwt= require('jsonwebtoken');
const config= require('../config/config');

//Crear un token
function createToken(user) {
    return jwt.sign({ id: user.id, email: user.email}, config.jwtsecret, {
        expiresIn: 60 * 60 * 24
    });
}


//Registro
exports.registerUser= (req,res)=>{
    if (!req.body.email || !req.body.password) {
        return res.status(400).json({'msg': 'Coloca un email y un password.'})
    }

    User.findOne({email: req.body.email}, (err,user)=>{
        if(err){
            return res.status(400).json({'msg': err});
        }

        if(user){
            return res.status(400).json({'msg': 'Este usuario ya existe'});
        }


        let newUser= User(req.body);
        newUser.save((err,user)=>{
            if (err) {
                return res.status(400).json({'msg': e});
            }

            return res.status(201).json(user);
        });
    })

};


//Login
exports.loginUser= (req,res)=>{
    if (!req.body.email || !req.body.password) {
        return res.status(400).json({'msg': 'Coloca un email y un password.'})
    }

    User.findOne({email: req.body.email}, (err,user)=>{
        if(err){
            return res.status(400).json({'msg': err});
        }

        if(!user){
            return res.status(400).json({'msg': 'Este usuario no existe'});
        }


        //verificar password
        user.comparePassword(req.body.password, (err,isMatch)=>{
            if (isMatch && !err) {
                return  res.status(200).json({
                    token :createToken(user)
                });
            }else {
                return res.status(400).json({msg: 'El email o password no coincide'});
            }
        })
    })

};