const express= require('express');
 routes= express.Router();

 var userController= require('./controller/userController');
 var passport= require('passport');


 routes.get('/', (req,res)=>{
     return res.send('Hola :v')
 });

 routes.post('/registro',userController.registerUser);
 routes.post('/login',userController.loginUser);

 routes.get('/info', passport.authenticate('jwt', {session:false}),(req,res)=>{
     return res.json({msg: `Hey ${req.user.email}! Esta disponible esta seccion para ti`});
 })
 module.exports=routes;