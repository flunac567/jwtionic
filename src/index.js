const express= require('express');
const bodyParser= require('body-parser');
var passport= require('passport');
const mongoose=require('mongoose');
const config= require('./config/config');
const port= process.env.PORT|| 5000;
var cors        = require('cors');
const app= express();
app.use(cors());

app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

app.use(passport.initialize());
var passportMiddleware= require('./middlewares/passport');
passport.use(passportMiddleware);


app.get('/', function (req,res) {
    return res.send('Hello! The API is at http://localhost:' + port + '/api');
    
});

const routes= require('./route');
app.use('/api',routes);


//Conexion MongoDB
mongoose.connect(config.db, {useNewUrlParser: true, useCreateIndex: true,useUnifiedTopology:true});

const connection=mongoose.connection;

connection.once('open', ()=>{
    console.log('MongoDB esta conectado');
});

connection.on('error',()=>{
    console.log('F por la conexion :v '+err);
    process.exit();
})

app.listen(port);
console.log('Estoy en el puerto '+port);